/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import UploadPhoto from './src/pages/UploadPhoto'
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => UploadPhoto);
