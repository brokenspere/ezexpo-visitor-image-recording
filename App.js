
'use strict';
import React, { PureComponent } from 'react';
import { AppRegistry, StyleSheet, Text, TouchableOpacity, View, CameraRoll, Image } from 'react-native';
import { RNCamera } from 'react-native-camera';
import _ from 'lodash';
import axios from 'axios';

import RNFetchBlob from 'react-native-fetch-blob'
import * as firebase from 'firebase'


// const firebaseConfig = {
//   apiKey: "AIzaSyCZ6l1JiMZpdFG4JO858OQayVROCsHtDAM",
//   authDomain: "ezexpo-firebase.firebaseapp.com",
//   databaseURL: "https://ezexpo-firebase.firebaseio.com",
//   projectId: "ezexpo-firebase",
//   storageBucket: "ezexpo-firebase.appspot.com",
//   messagingSenderId: "446334930491",
//   appId: "1:446334930491:web:1b9a9440a07f7218"
// };
// // Initialize Firebase
// firebase.initializeApp(firebaseConfig);
let i = 0

class ExampleApp extends PureComponent {

  constructor(props) {
    super(props)
    this.state = {
      images: [],
      dp: null
      // uploadUrl : 'http://10.80.127.72:8080/uploadFile'
    }
  }


  
  takePicture = async () => {
    
    const Blob = RNFetchBlob.polyfill.Blob
    const fs = RNFetchBlob.fs
    window.XMLHttpRequest = RNFetchBlob.polyfill.XMLHttpRequest
    window.Blob = Blob
    const uid = "visitor image"


    if (this.camera) {
      const options = { quality: 0.5, base64: true };
      //const temp = _.cloneDeep(this.state.images);
      //console.log(temp);

      await this.camera.takePictureAsync(options).then(async (data) => {
        // ToastAndroid.show(data.uri, ToastAndroid.SHORT);
        // CameraRoll.saveToCameraRoll(data.uri);
       
        const imagePath = data.uri
        const imageRef = firebase.storage().ref(uid).child("Test image"+i)
        let mime = 'image/jpg'
        let uploadBlob = null;
        fs.readFile(imagePath, 'base64')
          .then((data) => {
            console.log(data+"data")
            return Blob.build(data, { type: `${mime};BASE64` })
          })
          .then((blob) => {
            console.log("blob1")
            uploadBlob = blob
            return imageRef.put(blob, { contentType: mime })
            
          })
          .then(() => {
            console.log("blob2")
            uploadBlob.close()
            return imageRef.getDownloadURL()
          })
          .then((url) => {
            let userData = {}
            let obj = []

            obj["dp"] = url
            this.setState(obj)
            console.log(this.state.dp)
            i=i+1
            console.log(i)
            
          })
          .catch((error) => {
            console.log(error)
          })
         
          // .then(()=>{this.setState({
          //   images: _.concat(temp, {
          //     name: "image" + (this.state.images.length + 1),
          //     filename: "image" + (this.state.images.length + 1) + ".jpg",
          //     filepath: data.uri,
          //     filetype: 'image/jpg'
          //   })})
         
          
        });
    
        

        //  var formData  = new FormData();
        //  formData.append('file', this.state.images)
        //  await fetch('http://10.80.127.72:8080/booth/userId?id=27')
        //  const form = new FormData();
        //   form.append('file', {
        //     uri: data.uri,
        //     type: 'image/jpeg', // <-- this
        //     name: "image"+(this.state.images.length+1)+".jpg"
        //   });
        //   axios.post("http://10.80.127.220:8080/uploadFile", form)
        // //  await fetch("http://10.80.127.72:8080/uploadFile", {
        // //   method: "POST",
        // //   headers:{"Content-Type": "multipart/form-data;boundary=gc0p4Jq0M2Yt08jU534c0p"},
        // //   body: formData,

        // // })
        // .then(response => console.log('upload success', response))
        // .catch(error => {
        //   console.log("upload error", error);
        //   alert("Upload failed!");
        // });

      // }).then(data => {



      // })
      // })
    }
  };
  startTakePhoto = () => {
    this.timer = setInterval(() => this.takePicture(), 5000)
    
  }
  stopTakePhoto = () => {
    clearInterval(this.timer)
    //console.log(this.state.images)
    
  }
  render() {
    return (
      //<Image source={{uri: 'file:///data/user/0/com.ezexpocamera/cache/Camera/abe72169-081d-41a9-839e-10cc7eaf0b2a.jpg'}}></Image>
      <View style={styles.container}>
        <RNCamera
          ref={ref => {
            this.camera = ref;
          }}
          style={styles.preview}
          type={RNCamera.Constants.Type.back}
          flashMode={RNCamera.Constants.FlashMode.auto}
          androidCameraPermissionOptions={{
            title: 'Permission to use camera',
            message: 'We need your permission to use your camera',
            buttonPositive: 'Ok',
            buttonNegative: 'Cancel',
          }}
          androidRecordAudioPermissionOptions={{
            title: 'Permission to use audio recording',
            message: 'We need your permission to use your audio',
            buttonPositive: 'Ok',
            buttonNegative: 'Cancel',
          }}
        // onGoogleVisionBarcodesDetected={({ barcodes }) => {
        //   console.log(barcodes);
        // }}
        />
        <View style={{ flex: 0, flexDirection: 'row', justifyContent: 'center' }}>
          <TouchableOpacity onPress={this.startTakePhoto} style={styles.capture}>
            <Text style={{ fontSize: 14 }}> START </Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={this.stopTakePhoto} style={styles.capture}>
            <Text style={{ fontSize: 14 }}> STOP </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }




}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'black',
  },
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  capture: {
    flex: 0,
    backgroundColor: '#fff',
    borderRadius: 5,
    padding: 15,
    paddingHorizontal: 20,
    alignSelf: 'center',
    margin: 20,
  },
});


export default ExampleApp;