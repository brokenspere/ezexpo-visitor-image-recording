'use strict';
import React, { PureComponent } from 'react';
import { AppRegistry, StyleSheet, Text, TouchableOpacity, View, Image, TextInput } from 'react-native';
import CameraRoll from "@react-native-community/cameraroll";
import { RNCamera } from 'react-native-camera';
import _ from 'lodash';
import axios from 'axios';
import Imagepicker from 'react-native-image-crop-picker'
import RNFetchBlob from 'react-native-fetch-blob'
import * as firebase from 'firebase'
import { whileStatement } from '@babel/types';
import { moveFile } from 'react-native-fs';

const Blob = RNFetchBlob.polyfill.Blob
const fs = RNFetchBlob.fs
window.XMLHttpRequest = RNFetchBlob.polyfill.XMLHttpRequest
window.Blob = Blob

class UploadPhoto extends PureComponent {
  constructor(props) {
    super(props)
    this.state = {
      pickPhoto: [],
      emotion:[],
      boothId: ''
    }
  }
 


  takePicture = async () => {
   
    if (this.camera) {
      const options = { quality: 0.5 };
      const data = await this.camera.takePictureAsync(options).then(data => {
        console.log(data['uri']);
          CameraRoll.saveToCameraRoll(_.toString(data['uri']), 'photo').catch(err => {
            console.warn(err);
            
          });
      });
      console.log(data.uri);
    }
  };
  startTakePhoto = () => {
    this.timer = setInterval(() => this.takePicture(), 5000)
  }

  onIdChange = text => {
    const boothId = text;
    this.setState({ boothId });
};
  stopTakePhoto = () => {
    clearInterval(this.timer)


  }

  uploadEmotion = ()=>{
    console.log(this.state.emotion)
     axios({
      url: `http://10.80.126.19:8080/addVisitorEmotion?id=${this.state.boothId}`,
      method: 'post',
      data: {
        happy: this.state.emotion.happy,
        sad: this.state.emotion.sad,
        surprise: this.state.emotion.surprise,
        angry: this.state.emotion.angry,
        disgust: this.state.emotion.disgust,
        fear: this.state.emotion.fear,
        netural: this.state.emotion.neutral,
        total: this.state.emotion.total,
      }
      
  }).then(()=>{
    alert('Upload sucess')
    console.log('success')
  }).catch(err => {
      console.warn("upload error", JSON.stringify(err, null, 4));
      alert('Cannot upload')
      return;
  });
  }

  sendPicture = () => {
    console.log(this.state.pickPhoto)
    const temp = _.cloneDeep(this.state.pickPhoto);
    console.log(temp);
    const form = new FormData();
    _.forEach(this.state.pickPhoto.edges, (val, key) => {
      form.append('file', {
        uri: val.node.image.uri,
        type: val.node.type, // <-- this
        name: val.node.timestamp + ".jpg"
      });
    })
    console.log(form)
    axios.post("https://flaskdetectimagescript1.herokuapp.com/upload", form)
      .then(response => this.setState({emotion : response.data})
      )
      .catch(error => {
        console.warn("upload error", JSON.stringify(error, null, 4));
        alert('cannot upload')
        return;
      }).then(()=>{
        this.uploadEmotion()
      })
      
  }
  openGallery = () => {
    CameraRoll.getPhotos({
      first: 10000,
      assetType: 'Photos',
      groupName:"DCIM"
    }).then(images => {
      this.setState({ pickPhoto: images })
    }).then(() => {
      this.sendPicture()
    })

  }
  render() {
    const { boothId} = this.state
    
    return (
      <View style={styles.container}>
        <RNCamera
          ref={ref => {
            this.camera = ref;
          }}
          style={styles.preview}
          type={RNCamera.Constants.Type.back}
          flashMode={RNCamera.Constants.FlashMode.off}
          androidCameraPermissionOptions={{
            title: 'Permission to use camera',
            message: 'We need your permission to use your camera',
            buttonPositive: 'Ok',
            buttonNegative: 'Cancel',
          }}
          androidRecordAudioPermissionOptions={{
            title: 'Permission to use audio recording',
            message: 'We need your permission to use your audio',
            buttonPositive: 'Ok',
            buttonNegative: 'Cancel',
          }}
        />

        <View style={{ flex: 0, flexDirection: 'row', justifyContent: 'center' }}>
          <TextInput style={styles.input}
            underlineColorAndroid="transparent"
            onChangeText={this.onIdChange}
            value={boothId} />
          <TouchableOpacity onPress={this.setBoothId} style={styles.capture}>
            <Text style={{ fontSize: 14 }}> set id </Text>
          </TouchableOpacity>

        </View>

        <View style={{ flex: 0, flexDirection: 'row', justifyContent: 'center' }}>
          <TouchableOpacity onPress={this.startTakePhoto} style={styles.capture}>
            <Text style={{ fontSize: 14 }}> START </Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={this.stopTakePhoto} style={styles.capture}>
            <Text style={{ fontSize: 14 }}> STOP </Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={this.openGallery} style={styles.capture}>
            <Text style={{ fontSize: 14 }}> Upload </Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'black',
  },
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  capture: {
    flex: 0,
    backgroundColor: '#fff',
    borderRadius: 5,
    padding: 15,
    paddingHorizontal: 20,
    alignSelf: 'center',
    marginLeft: 15,
    marginRight: 5
  },
  input: {
    flex: 0,
    margin: 20,
    height: 40,
    width: 200,
    color: 'white',
    borderColor: 'white',
    alignSelf: 'center',
    borderWidth: 1
  }
});

export default UploadPhoto;